
<!-- README.md is generated from README.Rmd. Please edit that file -->

# drat

This drat for both insertion of packages, and usage from R. Current
Content Please see here for packages currently in the ghrr repo. Setup
Simply install the drat CRAN package the usual way via
install.packages(“drat”, repos=“<http://cran.rstudio.com>”). Add one
line to your .Rprofile file (or to Rprofile.site): drat:::add(“ghrr”)
That’s it\! Now use install.packages() or update.packages() and the ghrr
will be considered just like the standard repositories for R. More You
can learn more about drat from the vignettes Drat for Package Users and
Drat for Package Authors.
